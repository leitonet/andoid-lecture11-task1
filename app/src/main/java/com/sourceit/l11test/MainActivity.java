package com.sourceit.l11test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.airplane_mode_state)
    TextView airplaneState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

       // LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("custom-event-name"));
    }

    BroadcastReceiver onNotice = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!= null) {
                boolean message = intent.getExtras().getBoolean("airplane");
                String state = message ? "ON" : "OFF";
                Log.d("receiver", "airplane mode changed");
                airplaneState.setText(state);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onNotice);
    }
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter("my.custom.INTENT");
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff);
    }

 /*   private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean message = intent.getExtras().getBoolean("airplane");
            String state = message ? "ON" : "OFF";
            Log.d("receiver", "airplane mode changed");
            airplane.setText(state);
        }
    };

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }*/
}
