package com.sourceit.l11test;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AirplaneReceiver extends BroadcastReceiver{

    public static final int ID = 101;
    public static final int NOTIFY_ID = 1001;

    @Override
    public void onReceive(Context context, Intent intent) {

        String state = isAirplaneModOn(context) ? "AirplaneMode Enable" : "AirplaneMode Disable";
        Log.d("MyReceiver", "onReceive");
        Toast.makeText(context, state, Toast.LENGTH_SHORT).show();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setLights(Color.GREEN, 500, 500)
                .setContentTitle("Airplane Mode")
                .setContentText("Большой брат следит за тобой");

        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);

//        Log.d("sender", "Broadcasting message");
//        Intent intentSend = new Intent("custom-event-name");
//        intent.putExtra("airplane", isAirplaneModOn(context));
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        Intent i = new Intent();
        i.setAction("my.custom.INTENT");
        i.putExtra("airplane", isAirplaneModOn(context));
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.sendBroadcast(i);
    }


    private boolean isAirplaneModOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}
